<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use App\Entity\Article;

class ArticleType extends AbstractType{

  public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
  {
    $builder->add("titre", TextType::class)
    ->add("apercu", TextType::class)
    ->add("contenu", TextType::class)
    ->add("titre", TextType::class)
    ->add("auteur", TextType::class);
  }
  
    public function configureOptions(\Symfony\component\OptionsResolver\optionsResolver $resolver){
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}