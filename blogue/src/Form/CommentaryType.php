<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use App\Entity\Commentary;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class CommentaryType extends AbstractType{

  public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add("pseudo", TextType::class)
      ->add("age", IntegerType::class)
      ->add("contenu", TextType::class);
      // ->add("idarticle", IntegerType::class);
  }
  
    public function configureOptions(\Symfony\component\OptionsResolver\optionsResolver $resolver){
        $resolver->setDefaults([
            "data_class" => Commentary::class
        ]);
    }
}