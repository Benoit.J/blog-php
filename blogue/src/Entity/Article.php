<?php

namespace App\Entity;

class Article {

    public $id;
    public $auteur;
    public $titre;
    public $contenu;
    public $apercu;
}