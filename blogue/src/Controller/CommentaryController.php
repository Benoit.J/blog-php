<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CommentaryRepository;
use App\Form\CommentaryType;
use App\Entity\Commentary;



class CommentaryController extends AbstractController{

    /**
     * @Route("/add-commentary/{id}", name="add_commentary")
     */
    public function index(int $id, Request $request, CommentaryRepository $repo)
    {

        $commentary = new Commentary();
        $commentary->idarticle=$id;
        $form = $this->createForm(CommentaryType::class, $commentary);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $repo->add($commentary);
            return $this->redirectToRoute("all_commentary");
        } 
        return $this->render("add-commentary.html.twig", [
            "form" => $form->createView(),  
            "commentary" => $commentary

        ]);
    }
    
    /**
     * @Route("/show-commentary/{id}", name="show_commentary")
     */
    public function oneCommentary(CommentaryRepository $repo, Int $id){
        return $this->render("show-commentary.html.twig",[
            "commentary" => $repo->find($id)
        ]);
    }

    /**
     * @Route("/delete-commentary/{id}", name="delete_commentary")
     */
    public function deleteCommentary(CommentaryRepository $repo, Int $id){
        $commentary = $repo->find($id);
        $repo->remove($commentary);
        return $this->redirectToRoute("all_commentary");
    }

}
