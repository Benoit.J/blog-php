<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use App\Repository\CommentaryRepository;



class ArticleController extends AbstractController{

    /**
     * @Route("/add-article", name="add_article")
     */
    public function index(Request $request, ArticleRepository $repo)
    {

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $repo->add($article);
            return $this->redirectToRoute("all_article");
            
        } 
        return $this->render("add-article.html.twig", [
            "form" => $form->createView(),  
            "article" => $article
            
        ]);
    }
    /**
     * @Route("/", name="all_article")
     */
    public function showArticles(ArticleRepository $repo, CommentaryRepository $repoC){
        
        return $this->render("all-articles.html.twig", [
        "articles" => $repo->findAll(),
        

    ]);
    }
    
    /**
     * @Route("/show-article/{id}", name="show_article")
     */
    public function oneArticle(ArticleRepository $repo, Int $id, CommentaryRepository $repoC){
        return $this->render("show-article.html.twig",[
            "article" => $repo->find($id),
            "comment" => $repoC->findAll($id)
        ]);
    }

    /**
     * @Route("/delete-article/{id}", name="delete_article")
     */
    public function deleteArticle(ArticleRepository $repo, Int $id){
        $article = $repo->find($id);
        $repo->remove($article);
        return $this->redirectToRoute("all_article");
    }
    
}
