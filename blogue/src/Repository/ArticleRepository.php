<?php

namespace App\Repository;

use App\Entity\Article;
use App\Repository\ConnectionUtil;

class ArticleRepository {

    public function findAll():array {
        $articles = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM article");
        $query->execute();
        $results = $query->fetchAll();
        foreach ($results as $line) {

            $articles[] = $this->sqlToArticle($line);
        }
        return $articles;
    }
    public function add(Article $article): void {
        
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO article (auteur, titre, contenu, apercu) 
                                       VALUES (:auteur, :titre, :contenu, :apercu)");

        $query->bindValue(":auteur", $article->auteur, \PDO::PARAM_STR);
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
        $query->bindValue(":contenu", $article->contenu, \PDO::PARAM_STR);
        $query->bindValue(':apercu', $article->apercu, \PDO::PARAM_STR);
        $query->execute();


        $article->id = $connection->lastInsertId();
    }

    public function find(int $id): ?Article {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM article WHERE id=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        if($line = $query->fetch()) {
            return $this->sqlToArticle($line);

        }
        return null;
    }


    public function update(Article $article): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE article SET auteur=:auteur, contenu=:contenu WHERE id=:id");
        $query->bindValue(":auteur", $article->auteur, \PDO::PARAM_STR);
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
        $query->bindValue(":contenu", $article->contenu, \PDO::PARAM_STR);

        $query->execute();

    }

    public function remove(Article $article): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM article WHERE id=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }

    private function sqlToArticle(array $line): Article {

        $article = new Article();
        $article->id= intval($line["id"]);
        $article->auteur = $line["auteur"];
        $article->titre = $line["titre"];
        $article->contenu = $line["contenu"];
        $article->apercu = $line["apercu"];

        return $article;
    }
}