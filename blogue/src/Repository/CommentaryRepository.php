<?php

namespace App\Repository;

use App\Entity\Commentary;
use App\Repository\ConnectionUtil;

class CommentaryRepository {

    public function findAll(int $id):array {
        $commentaries = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM commentary WHERE idarticle=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        $results = $query->fetchAll();
        foreach ($results as $line) {
            $commentaries[] = $this->sqlToCommentary($line);
        }
        return $commentaries;
    }
    public function add(Commentary $commentary): void {
        
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO commentary (pseudo, age, contenu, idarticle) 
                                       VALUES (:pseudo, :age, :contenu, :idarticle)");

        $query->bindValue(":pseudo", $commentary->pseudo, \PDO::PARAM_STR);
        $query->bindValue(":age", $commentary->age, \PDO::PARAM_INT);
        $query->bindValue(":contenu", $commentary->contenu, \PDO::PARAM_STR);
        $query->bindValue(":idarticle", $commentary->idarticle, \PDO::PARAM_INT);
        $query->execute();


        $commentary->id = $connection->lastInsertId();
    }

    public function find(int $id): ?Commentary {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM commentary WHERE idcommentary=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        if($line = $query->fetch()) {
            return $this->sqlToCommentary($line);

        }
        return null;
    }


    public function update(Commentary $commentary): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE commentary SET pseudo=:pseudo, age=:age, contenu=:contenu WHERE idcommentary=:id");
        $query->bindValue(":id", $commentary->idcommentary, \PDO::PARAM_INT);
        $query->bindValue(":auteur", $commentary->auteur, \PDO::PARAM_STR);
        $query->bindValue(":titre", $commentary->titre, \PDO::PARAM_STR);
        $query->bindValue(":contenu", $commentary->contenu, \PDO::PARAM_STR);

        $query->execute();

    }

    public function remove(Commentary $commentary): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM commentary WHERE idcommentary=:id");
        $query->bindValue(":id", $commentary->idcommentary, \PDO::PARAM_INT);

        $query->execute();
    }

    private function sqlToCommentary(array $line): Commentary {

        $commentary = new Commentary();
        $commentary->idcommentary= intval($line["idcommentary"]);
        $commentary->pseudo = $line["pseudo"];
        $commentary->age = $line["age"];
        $commentary->contenu = $line["contenu"];
        $commentary->idarticle = intval($line["idarticle"]);

        return $commentary;
    }
}